package programacionAplicada.redSocial.vista;

import java.util.InputMismatchException;
import java.util.Scanner;
import programacionAplicada.redSocial.Exception.UsuarioException;
import programacionAplicada.redSocial.modelo.*;

public class Vista {
    
	private RedSocial modelo;
	private Scanner teclado ;
	
	public Vista(RedSocial modelo){
		this.modelo = modelo;
		this.teclado = new Scanner(System.in);
	}
	
	public void index() throws InputMismatchException{
            int menu=0;
            
           while(menu!=2){
	    int opcionMenu =-1;
            
		System.out.println("Bienvenido a la red Social");
                
		System.out.println("1. Registrarse");
                System.out.println("2. Login");
                System.out.println("3. Salir");
                
                try{
                System.out.println("Eleccion -> ");
	        opcionMenu = this.teclado.nextInt();
                } catch(InputMismatchException e){
                    System.out.println("Ingrese un numero");
                    opcionMenu=3;
                }
                
		switch(opcionMenu){
		case 1:
                    
		    try{
                 this.registrarUsuario();
                    }catch(UsuarioException e){
                        System.out.println(e.getMessage());
                    }catch(InputMismatchException e){
                        System.out.println(e.getMessage());
                    }
                    catch(IllegalArgumentException e){
                        System.out.println(e.getMessage());
                    }
			break;		
		case 2:
		    this.Login();
			break;
                case 3:
                    menu=2;
                    break;
		}
        }
	}
	
	public void registrarUsuario()throws UsuarioException,InputMismatchException, IllegalArgumentException {
            boolean UsuarioR=false;
            int edad=0;
            String correo="";
            String clave="";
            
		System.out.println("Nombre");
		String nombre=this.teclado.next();
                if(nombre.length()>100)
                throw new UsuarioException("Tiene que ser un nombre real menor a 100 caracteres");
                
                
               
                try{
                   System.out.println("Edad");
		   edad= this.teclado.nextInt();
                }catch(InputMismatchException e){
                    System.out.println("Caracter no valido"); 
                }
                
                 if(edad<18){
                 throw new UsuarioException("Debes ser mayor de 18 años"); 
                 }
                
                System.out.println("Correo");
                
		correo = this.teclado.next();                
		
                System.out.println("Nit");
		String nit= this.teclado.next();
                
		System.out.println("Clave");
               
                 clave=this.teclado.next();
                
                
               if (clave.equals("123456")){
                 throw new IllegalArgumentException("La contraseña no puede ser 123456");
                }
                
                
		Usuario usuario = new Usuario(nombre,edad,correo,nit,clave);
		this.modelo.agregarUsuario(usuario);
                UsuarioR=true;
                if (UsuarioR){
                
                this.Login();
                
                }
        }
	
	public void Login(){
            
            boolean UsuarioL=false;
            int respuestaLogin=0;
            
            while(respuestaLogin!=2){
            int respuesta=0;
            
            while (respuesta!=2){
            System.out.println(" Inicia sesion, Ingresa el NickName de tu Usuario");
            String NickName=this.teclado.next();
            
            for(int i=0; i<this.modelo.getUsuario().size();i++){  
                
            if(this.modelo.getUsuario().get(i).getNit().equals(NickName)){
                System.out.println("Usuario Encontrando");
                
                int respuestapass=0;
                while(respuestapass!=2){
                System.out.println("Ingrese la clave del usuario "+NickName);
                String pass=this.teclado.next();
               // for (i=0; i<this.modelo.getUsuario().size();i++){
                if(this.modelo.getUsuario().get(i).getClave().equals(pass)){
                    System.out.println("Usted esta logeado");
                    respuestapass=2;
                    UsuarioL=true;
                    } else {
                    try{
                    System.out.println(" Error en la clave, deseas volver a intentarlo? 1.Si 2.No");
                    respuestapass=this.teclado.nextInt();
                    }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
                }
                //}
            }
            
            } else {
                System.out.println("Usuario no encontrado");
            }
            
            }
            if(UsuarioL){
                respuestaLogin=2;
                respuesta=respuestaLogin;
            }else{
                try{
                System.out.println("Buscar otra vez? 1. Si 2. No");
                respuestaLogin=this.teclado.nextInt();
                respuesta=respuestaLogin;
                }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
            }
            }
        }
            
            if (UsuarioL){
                 int menu=0;
            while(menu!=2){
	    int opcionMenu = -1;
                System.out.println("__________**** FACEROOK****__________");
                System.out.println("1. Realizar Comentario");
                System.out.println("2. Subir Fotografia");
                System.out.println("3. Buscar Usuarios");
                System.out.println("4. Listar Comentarios");
                System.out.println("5. Listar Fotos Donde Estas Etiquetado");
                System.out.println("6. Buscar Palabra en Comentarios");
                System.out.println("7. Salir");
                System.out.println(" Eleccion -> ");
                
	    opcionMenu = this.teclado.nextInt();
            
		switch(opcionMenu){
                
                case 1:
                    try{
                    this.realizarComentario();
                    }catch(InputMismatchException e){
                        System.out.println(e.getMessage());
                    }
                    catch(UsuarioException e){
                        System.out.println(e.getMessage());
                    }
                    
                    break;
                case 2:
                    this.subirFoto();
                    break;
                case 3:
                    this.buscarUsuario();
                    break;
                case 4:
                    this.listarComentarios();
                    break;
                case 5:
                    this.etiquetasPersonales();
                    break;
                case 6:
                    this.busquedaPalabra();
                    break;
                case 7:
                    menu=2;
                    break;
                
                
                }
                
                
            }
                }
                
            
            
        }
        
        public void realizarComentario() throws InputMismatchException, UsuarioException{
            
            Comentario comentariorealizado = new Comentario();
            int respuesta=0;
            String fechacomentario="";
            
            while(respuesta!=2){  
                
            System.out.println("Escribe Aqui Sin Dejar Espacios: ");
            String textocomentario=this.teclado.next();
            comentariorealizado.setTexto(textocomentario);
            
            if(textocomentario.length()>200){
            throw new UsuarioException("El comentario excede los 200 caracteres.");
            }
            
            System.out.println("Ingrese La Fecha Del Comentario: ");
            fechacomentario=this.teclado.next();
            comentariorealizado.setFecha(fechacomentario);
           
            
            
            System.out.println("Ingrese El Nombre Del Usuario a Guardar comentario");
            String User=this.teclado.next();
            int Rev=0;
            while(Rev!=2){
            for (int i=0; i<this.modelo.getUsuario().size();i++){
                if(this.modelo.getUsuario().get(i).getNombre().equals(User)){
                    System.out.println("El Usuario Existe");                 
                                       
                    this.modelo.agregarComentario(comentariorealizado, this.modelo.getUsuario().get(i));
                    // this.modelo.getUsuario().get(i).addComentario(comentariorealizado); agregarComentario ya tiene addComentario
                    System.out.println("Comentario Guardado a "+User);
                    Rev=2;
                    } else {
                   
                    Rev=0;
                }
            }
            
            Rev=2;
            
            
            try{
                System.out.println("Deseas realizar otro comentario? 1. Si 2. No");
                respuesta=this.teclado.nextInt();
                
            }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                        
                    }
            
                if(respuesta==2){
                Rev=2;
                }
                
            
            }     
        }   
            
        }
        
        public void subirFoto(){
            
            
            
            System.out.println("Ingrese El Nombre Del Archivo: ");
            String nombreArchivo=this.teclado.next();
            
            System.out.println("Ingrese La Fecha: ");
            String fecha=this.teclado.next();
            //El Usario de Origen No Se Reconoce
            
            
            int whiile=0;
            while(whiile!=2){
                System.out.println("Ingrese el nombre de su Usuario");    
                String propiedad=this.teclado.next();
            for (int i=0; i<this.modelo.getUsuario().size();i++){
                if(this.modelo.getUsuario().get(i).getNombre().equals(propiedad)){
                    System.out.println("Hola "+propiedad);
                    whiile=2;
//            System.out.println("Desea agregar descripcion a la foto? 1. Si 2. No");
//                int respuestadescripcion=this.teclado.nextInt();
//               
//            if(respuestadescripcion==1){
//                System.out.println("Ingrese La Descripcion De La Fotografia: ");
//                String descripcion=this.teclado.next();
//                
//                Fotografia foto = new Fotografia(nombreArchivo,fecha,descripcion);
//                this.modelo.getUsuario().get(i).addFotografia(foto);
//                whiile=2;
//            } else {
//                
//                Fotografia foto = new Fotografia(nombreArchivo,fecha,null);
//                this.modelo.getUsuario().get(i).addFotografia(foto);
//                whiile=2;
//             }
                }else {
                    System.out.println("Usuario no encontrado");
                    whiile=0;
                }
            }
        }
    
            System.out.println("Desea realizar alguna etiqueta? 1. Si 2. No");
            int respuestaetiqueta=0;
            try{
            respuestaetiqueta=this.teclado.nextInt();
            }catch (InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero"); 
                    }
            if (respuestaetiqueta==1){
                
                Etiqueta[] etiquetas = new Etiqueta[5];
                int respuesta=0;  
                int i=0;
                
                while (respuesta!=2 & i<5){
                    
                System.out.println("Que usuario desea etiquetar?");
                String usuarioetiquetado=this.teclado.next();
                int respuesta1=0;
                
                while(respuesta1!=2){
                    
                for (i=0; i<this.modelo.getUsuario().size();i++){
                    
                if(this.modelo.getUsuario().get(i).getNombre().equals(usuarioetiquetado)){
                    System.out.println("Usuario encontrado");
                 
                         
                System.out.println("Que Coordenada en X desea para "+usuarioetiquetado+" ?" );
                int coordenadaX = this.teclado.nextInt();
                
                System.out.println("Que coordenada en Y desea para "+usuarioetiquetado+" ?" );
                int coordenadaY = this.teclado.nextInt();
                
                etiquetas[i]= new Etiqueta(coordenadaX, coordenadaY, this.modelo.getUsuario().get(i));
                this.modelo.getUsuario().get(i).getFotografias().get(i).agregarEtiquetas(etiquetas);
                //////////////////////////////////////////////////////////////////////////////////////// Utilizar el metodo agregar etiqueta
                    System.out.println("Etiqueta Realizada Con Exito");
                    respuesta1=2;
                    } else {
                    System.out.println(" Usuario No Encontrado, etiqueta no guardada");
                    respuesta1=2;
                }
                }
                    
                }
                
                    i++;     
                    try{
                    System.out.println("Deseas volver a etiquetar? 1.Si 2.No");
                    respuesta=this.teclado.nextInt();
                    }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
                    if(i>4){
                        System.out.println("No puedes realizar mas etiquetas ");
                    }
                }
                
           }else{
                int intento=0;
                int respuestadescripcion=0;
                try{
                System.out.println("Desea agregar descripcion? 1. Si 2. No");
                respuestadescripcion=this.teclado.nextInt();
                }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
            if(respuestadescripcion==1){
                System.out.println("Ingrese La Descripcion De La Fotografia: ");
                String descripcion=this.teclado.next();
                Fotografia foto = new Fotografia(nombreArchivo,fecha,descripcion);
                
                while(intento!=2){
                System.out.println("Ingrese el nombre de su Usuario");
                String NickName=this.teclado.next();
            
            for(int i=0; i<this.modelo.getUsuario().size();i++){  
            if(this.modelo.getUsuario().get(i).getNombre().equals(NickName)){
                System.out.println("Usuario Encontrando");
                this.modelo.getUsuario().get(i).addFotografia(foto);
                intento=2;
            } else {
                System.out.println("Error en el Usuario, Vuelva a intentarlo");
                intento=0;
            }
            }
            }  
                
            } else {
                
                Fotografia foto = new Fotografia(nombreArchivo,fecha,null);
                
                while(intento!=2){
                System.out.println("Ingrese el NickName de su Usuario");
                String NickName=this.teclado.next();
            
            for(int i=0; i<this.modelo.getUsuario().size();i++){  
            if(this.modelo.getUsuario().get(i).getNit().equals(NickName)){
                System.out.println("Usuario Encontrando");
                this.modelo.getUsuario().get(i).addFotografia(foto);
                
                intento=2;
            } else {
                System.out.println("Error en el Usuario, Vuelva a intentarlo");
                intento=0;
            }
            }
            
            }
                
            }
            
            }
        }
        
        public void buscarUsuario(){
            int busqueda=0;
            while (busqueda!=2){
             System.out.println("Escribe El Nombre Del Usuario Que Deseas Buscar: ");
             String usuarioBuscado= this.teclado.next();
             this.modelo.getUsuario();
             
             for (int i = 0; i < this.modelo.getUsuario().size(); i++) {
		if(this.modelo.getUsuario().get(i).getNombre().equals(usuarioBuscado)){
		                  System.out.println("El usuario buscado existe: "+usuarioBuscado);
		} else {
                    System.out.println(" El usuario no existe... ");
                }
             }
             try{
                System.out.println("Deseas volver a buscar? 1. Si 2. No");
                busqueda= this.teclado.nextInt();
             }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
	 }
            
        }
        
        public void listarComentarios(){
            int respuesta=0;
            while(respuesta!=2){
            System.out.println("De que usuario deseas listar los comentarios?");
            String usuarioBuscado=this.teclado.next();
            
            for (int i = 0; i < this.modelo.getUsuario().size(); i++) {
		if(this.modelo.getUsuario().get(i).getNombre().equals(usuarioBuscado)){
		                  System.out.println("El usuario buscado existe: "+usuarioBuscado);
                                  
                                  Comentario coments=this.modelo.getUsuario().get(i).getComentarios().get(i);
                                  System.out.println(coments);
		} else {
                    System.out.println("... ");
                }
             }
            try{
                System.out.println("Volver a buscar comentarios? 1. Si 2.No");
                respuesta= this.teclado.nextInt();
            }catch(InputMismatchException e){
                        System.out.println("Debe Ingresar Un Numero");
                    }
        }
        }
        
        public void etiquetasPersonales(){
            int ciclo=0;
            while(ciclo!=2){
            System.out.println("Ingrese el nombre de su Usuario");
            String usuarioBuscado=this.teclado.next();
            
            for (int i = 0; i < this.modelo.getUsuario().size(); i++) {
		if(this.modelo.getUsuario().get(i).equals(usuarioBuscado)){
		                  System.out.println("Hola: "+usuarioBuscado);
                        //////////////////////////////////////////////////////////7          this.modelo.getUsuario().get(i). Entrar en getEtiquetas
                        System.out.println(this.modelo.getUsuario().get(i).getFotografias().get(i).getEtiquetas());
                        System.out.println("No hay mas etiquetas para mostrar");
                        ciclo=2;
		} else {
                    System.out.println(" El usuario no existe... ");
                    ciclo=0;
                }
             }
        }
        }
        
        public void busquedaPalabra(){
            int ciclo=0;
            while(ciclo!=2){
            System.out.println("Ingrese la palabra a buscar");
            String palabra=this.teclado.next();
            
            for (int i = 0; i < this.modelo.getUsuario().size(); i++){
                if(this.modelo.getUsuario().get(i).getComentarios().get(i).equals(palabra)){
                    System.out.println("Se encontraron los siguientes resultados");
		                  System.out.println(this.modelo.getUsuario().get(i).getComentarios().get(i));
                                  System.out.println("No hay mas para mostrar ");
                                  ciclo=2;
		}  else {
                    try{
                     System.out.println("No se encontraron resultados...");
                    }catch(IndexOutOfBoundsException e){
                        
                    }
                     }   
             } 
        }
        }
        
	public static void main(String[] args) {
            
            RedSocial Mainn=new RedSocial();
            
            Vista Test=new Vista(Mainn);
            
            Test.index();
	}
	
}

