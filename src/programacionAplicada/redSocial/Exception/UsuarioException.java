
package programacionAplicada.redSocial.Exception;

public class UsuarioException  extends Exception{
    public UsuarioException(String mensaje){
      super(mensaje);
    }
}