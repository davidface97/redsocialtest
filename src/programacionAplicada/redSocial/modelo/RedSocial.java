package programacionAplicada.redSocial.modelo;
import java.util.ArrayList;

public class RedSocial {
	private String nombre;
	private ArrayList<Usuario>usuarios;

        public RedSocial (){
            this.nombre="";
            this.usuarios = new ArrayList<Usuario>();
        }
        
	public RedSocial(String nombre) {
		super();
		this.nombre = nombre;
		this.usuarios = new ArrayList<Usuario>();
	}
	
	public ArrayList<Usuario> getUsuario() {
		return usuarios;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void agregarUsuario(Usuario usuario){
	   this.usuarios.add(usuario);
	}
	
	public void agregarComentario(Comentario comentario, Usuario usuario){
	 for (int i = 0; i < this.usuarios.size(); i++) {
		if(this.usuarios.get(i).equals(usuario)){
		   this.usuarios.get(i).addComentario(comentario);	
		   break;
		}
	 }
    }
}
