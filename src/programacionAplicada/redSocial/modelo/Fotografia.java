package programacionAplicada.redSocial.modelo;

public class Fotografia {
	private String nombreArchivo;
	private String fecha;
	private String descripcion;
	private Etiqueta[] etiquetas;
	
	public Fotografia(String nombreArchivo, String fecha, String descripcion) {
		super();
		this.nombreArchivo = nombreArchivo;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.etiquetas = new Etiqueta[5]; 
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Etiqueta[] getEtiquetas() {
		return etiquetas;
	}

	public void agregarEtiquetas(Etiqueta[] etiquetas){
		//Validar que sea entre 2 y 5
		this.etiquetas=etiquetas;
	}

}
