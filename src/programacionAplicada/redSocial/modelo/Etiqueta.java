package programacionAplicada.redSocial.modelo;

public class Etiqueta {
    
	private int posX;
	private int posY;
	private Usuario usuario;
		
	public Etiqueta(int posX, int posY, Usuario usuario) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.usuario = usuario;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	
	

}
