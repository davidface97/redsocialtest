package programacionAplicada.redSocial.modelo;
import java.util.ArrayList;
public class Usuario {
    
	private String nombre;
	private int edad;
	private String correo;
	private String nit;
	private String clave;
	private ArrayList<Comentario>comentarios;
	private ArrayList<Fotografia>fotografias;
	
	
	public Usuario(String nombre, int edad, String correo, String nit,String clave) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.correo = correo;
		this.nit = nit;
		this.clave = clave;
		this.comentarios = new ArrayList<Comentario>();
		this.fotografias= new ArrayList<Fotografia>();
	}

	
        public void addComentario(Comentario comentario){
      this.comentarios.add(comentario);
	}	
	
	public void addFotografia(Fotografia fotografia){
		this.fotografias.add(fotografia);
	}
        
	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}


	public ArrayList<Fotografia> getFotografias() {
		return fotografias;
	}

	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}

	

}
